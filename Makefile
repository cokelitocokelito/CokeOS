CDIR = src
ASMDIR = src/asm

OBJECTS =	$(CDIR)/kmain.o					\
					$(CDIR)/Framebuffer.o 	\
					$(CDIR)/string.o				\
					$(CDIR)/COMPort.o				\
					$(CDIR)/stdio.o					\
					$(CDIR)/gdt.o						\
					$(ASMDIR)/loader.o			\
					$(ASMDIR)/io.o					\
					$(ASMDIR)/kernel.o			\

CC = gcc
CFLAGS = -m32							\
		 -nostdlib						\
		 -nostdinc						\
		 -fno-builtin					\
		 -fno-stack-protector	\
		 -nostartfiles				\
		 -nodefaultlibs				\
		 -Wall								\
		 -I include/					\
		 -c

LDFLAGS = -T link.ld -melf_i386
AS = nasm
ASFLAGS = -f elf32

all: kernel.elf

kernel.elf: $(OBJECTS)
	ld $(LDFLAGS) $(OBJECTS) -o kernel.elf

iso: kernel.elf
	@cp kernel.elf ./iso/boot/kernel.elf
	genisoimage -R                              	\
                -b boot/grub/stage2_eltorito    \
                -no-emul-boot                   \
                -boot-load-size 4               \
                -A os                           \
                -input-charset utf8             \
                -quiet                          \
                -boot-info-table                \
                -o cokeos.iso                 	\
                iso

run:
	bochs -f bochs.txt -q

%.o: %.c
	$(CC) $(CFLAGS) $< -o $@

%.o: %.asm
	$(AS) $(ASFLAGS) $< -o $@

clean:
	rm -fr $(CDIR)/*.o $(ASMDIR)/*.o cokeos.iso kernel.elf rm -fr iso/boot/kernel.elf
