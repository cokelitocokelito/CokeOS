/*
This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _COMPORT_H_
#define _COMPORT_H_

#include "io.h"
#include "types.h"

#define SERIAL_COM1_BASE				0x3F8
#define SERIAL_COM2_BASE				0x2F8
#define SERIAL_COM3_BASE				0x3E8
#define SERIAL_COM4_BASE				0x2E8

#define SERIAL_DATA_PORT(base)			(base)
#define SERIAL_FIFO_COMMAND_PORT(base)	(base + 2)
#define SERIAL_LINE_COMMAND_PORT(base)	(base + 3)
#define SERIAL_MODEM_COMMAND_PORT(base)	(base + 4)
#define SERIAL_LINE_STATUS_PORT(base)	(base + 5)

#define SERIAL_LINE_ENABLE_DLAB			0x80

void com_configure_baud_rate(u16 com, u16 divisor);
void com_configure_line(u16 com);
void com_write_char(u16 com, u8 data);
void com_write(u16 com, char *data, u16 size);
u32  com_is_transmit_fifo_empty(u16 com);

#endif //_COMPORT_H_
