/*
This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _FRAMEBUFFER_H_
#define _FRAMEBUFFER_H_

#define FB_BLACK		0
#define FB_BLUE			1
#define FB_GREEN		2
#define FB_CYAN			3
#define FB_RED			4
#define FB_MAGENTA		5
#define FB_BROWN		6
#define FB_LIGHTGREY	7
#define FB_DARKGREY		8
#define FB_LIGHTBLUE	9
#define FB_LIGHTGREEN	10
#define FB_LIGHTCYAN	11
#define FB_LIGHTRED		12
#define FB_LIGHTMAGENTA	13
#define FB_LIGHTBROWN	14
#define FB_WHITE		15

#define FB_DEFAULT_COLOR ((FB_BLACK & 0x0F) << 4 | (FB_WHITE & 0x0F))

#define FB_ADDRESS 0x000B8000

#define FB_COMMAND_PORT		0x3D4
#define FB_DATA_PORT		0x3D5

#define FB_HIGH_BYTE_CMD	14
#define FB_LOW_BYTE_CMD		15

#include "types.h"

/*
*	La funcion de este archivo es proporcionar una
*	manera eficiente para escribir en el Framebuffer
*	Author: Jorge Bravo
*/

extern u16 current_width;
extern u16 current_height;

/*
*	Bits 0-3 representa BG
*	Bits 4-7 representa FG
*	Bits 8-15 representa el caracter que se quiera
*/
struct framebuffer_character_t {
	u8 ASCIICode;
	u8 Colors;
} __attribute__((packed));

extern void framebuffer_write_cell(struct framebuffer_character_t, u16);
extern void framebuffer_init(struct framebuffer_character_t);
extern void framebuffer_move_cursor(u16);
extern void framebuffer_write(char *, u16);

#endif // _FRAMEBUFFER_H_
