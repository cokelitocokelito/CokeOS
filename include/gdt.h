/*
This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _GDT_H_
#define _GDT_H_

#include "types.h"

struct gdt_t {
	u32 address;
	u16 size;
}__attribute__((packed));

extern u64 gdt_table[3];

u64  create_descriptor(u32 base, u32 limit, u16 flag);
void init_gdt();
void load_gdt(struct gdt_t gdt_address);

#endif  //_GDT_H_
