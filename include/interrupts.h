/*
This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _INTERRUPT_H_
#define _INTERRUPT_H_

#include "types.h"

struct idt_entry_t {
	u16 offset_high;
	u8 DPL;
	u8 reserved;
	u16 segment_selector;
	u16 offset_low;
}__attribute__((packed));

struct idt_entry_t create_entry(u32 offset, u16 segment, u8 DPL);
void init_entry();
void load_idt(struct idt_entry_t *);

#endif  //_INTERRUPT_H_
