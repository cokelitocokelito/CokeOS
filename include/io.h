/*
This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _IO_H_
#define _IO_H_

#include "types.h"

/*
*	La funcion de este archivo es proporcionar
*	los prototipos para las funciones de I/O
*	que se implementan en ASM
*	Author: Jorge Bravo
*/

void outb(u16 port, u8 data);
u8   inb(u16 port);

#endif //_IO_H_
