/*
This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _KERNEL_H_
#define _KERNEL_H_

/*
*	La funcion de este archivo es implementar
*	funciones comunes para todas las arquitecutaras,
*	se debe intentar solamente ocupar datos primitivos
*	o en su defecto struct que esten definidas en todas las arquitecturas
*
*	Author: Jorge Bravo
*/

void kerror();

#endif  //_KERNEL_H_
