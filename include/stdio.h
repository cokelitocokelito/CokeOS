/*
This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _STDIO_H_
#define _STDIO_H_

#include "types.h"
#include "Framebuffer.h"
#include "COMPort.h"

#define FD_FRAMEBUFFER	0
#define FD_COM1			1
#define FD_COM2			2
#define FD_COM3			3
#define FD_COM4			4

/*
*	La funcion de este archivo es proporcionar
*	una interafaz comun para el output
*
*	Author: Jorge Bravo
*/

int write(char *buffer, u8 fd);

#endif  //_STDIO_H_
