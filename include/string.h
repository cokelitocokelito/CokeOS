/*
This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _STRING_H_
#define _STRING_H_

#include "types.h"

/*
*	La funcion de este archivo es proporcionar
*	funciones que nos seran util en el manejo
*	de string en c
*	Author: Jorge Bravo
*/

u16 strlen(const char *);
u16 strcmp(char *, const char *, u16);

void strcpy(char *, const char *, u16);

#endif //_STRING_H_
