/*
This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _TYPES_H_
#define _TYPES_H_

/*
*	La funcion de este archivo es proporcionar
*	tipos de variables de un determinado numeros de bytes
*	Author: Jorge Bravo
*/

typedef unsigned char		u8;
typedef unsigned short		u16;
typedef unsigned int		u32;
typedef unsigned long long	u64;

typedef signed char			s8;
typedef signed short		s16;
typedef signed int			s32;
typedef signed long long	s64;

typedef volatile u8			vu8;
typedef volatile u16		vu16;
typedef volatile u32		vu32;
typedef volatile u64		vu64;

typedef volatile s8			vs8;
typedef volatile s16		vs16;
typedef volatile s32		vs32;
typedef volatile s64		vs64;

#endif // _TYPES_H_
