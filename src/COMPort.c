/*
This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "COMPort.h"

void com_configure_baud_rate(u16 com, u16 divisor)
{
	outb(SERIAL_LINE_COMMAND_PORT(com), SERIAL_LINE_ENABLE_DLAB);
	outb(SERIAL_DATA_PORT(com), (divisor >> 8) & 0x00FF);
	outb(SERIAL_DATA_PORT(com), divisor & 0x00FF);
}

void com_configure_line(u16 com)
{
	outb(SERIAL_LINE_COMMAND_PORT(com), 0x03);
	outb(SERIAL_FIFO_COMMAND_PORT(com), 0xC7);
	outb(SERIAL_MODEM_COMMAND_PORT(com), 0x03);
}

u32 com_is_transmit_fifo_empty(u16 com)
{
	return inb(SERIAL_LINE_STATUS_PORT(com)) & 0x20;
}

void com_write_u8(u16 com, u8 data)
{
	outb(SERIAL_DATA_PORT(com), data);
}

void com_write(u16 com, char *data, u16 size)
{
	u16 buffer_index = 0;
	while (buffer_index < size) {
		com_write_u8(com, data[buffer_index]);
		buffer_index++;
	}
}
