/*
This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Framebuffer.h"
#include "io.h"

u16 current_width = 0;
u16 current_height = 0;

static struct framebuffer_character_t *framebuffer = (struct  framebuffer_character_t *) FB_ADDRESS;

void framebuffer_write_cell(struct framebuffer_character_t character, u16 position)
{
	framebuffer[position] = character;
}

void framebuffer_init(struct framebuffer_character_t character)
{
	for (int y = 0; y < 25; y++) {
		for (int x = 0; x < 80; x++) {
			framebuffer_write_cell(character, (y * 80) + x);
		}
	}

	current_height = 0;
	current_width = 0;
	framebuffer_move_cursor((current_height * 80) + current_width);
}

void framebuffer_move_cursor(u16 position)
{
	outb(FB_COMMAND_PORT, FB_HIGH_BYTE_CMD);
	outb(FB_DATA_PORT, ((position >> 8) & 0x00FF));
	outb(FB_COMMAND_PORT, FB_LOW_BYTE_CMD);
	outb(FB_DATA_PORT, position & 0x00FF);
}

void framebuffer_write(char *buffer, u16 len)
{
	struct framebuffer_character_t current_character;
	u16 index_buffer = 0;
	current_character.Colors = FB_DEFAULT_COLOR;


start:
	while (index_buffer < len) {
		if (current_width >= 80) {
			current_width = 0;
			current_height++;
		}

		current_character.ASCIICode = buffer[index_buffer];

		if (current_character.ASCIICode == '\n') {
			current_width = 0;
			current_height++;
			current_character.ASCIICode = ' ';
			index_buffer++;
			goto start;
		}

		if (current_height >= 25) {
			current_character.ASCIICode = ' ';
			framebuffer_init(current_character);
		}

		framebuffer_write_cell(current_character, (current_height * 80) + current_width);
		framebuffer_move_cursor((current_height * 80) + current_width);
		current_width++;
		index_buffer++;
	}
}
