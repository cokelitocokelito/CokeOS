;This program is free software: you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program.  If not, see <http://www.gnu.org/licenses/>.


; La funcion de este archivo sera proporcionar
; una manera de acceder desde c a funciones io
; sin la necesidad de inline asm
; Author: Jorge Bravo

global outb
global inb
global load_gdt

outb:						; Proporciona un wrapper para out
	mov al, [esp + 8]
	mov dx, [esp + 4]
	out dx, al
	ret

inb:						; Proporcionar un wrapper para in
	mov dx, [esp + 4]
	in al, dx
	ret

load_gdt:
	mov eax, [esp + 4]
	lgdt [eax]
	ret
