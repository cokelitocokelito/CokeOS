;This program is free software: you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program.  If not, see <http://www.gnu.org/licenses/>.




; La funcion de este archivo sera proporcionar
; una manera de cargar el kernel de CokeOs (Joix)
; en la memoria y dejar una stack lista para c
; Author: Jorge Bravo

global loader					;Punto de entrada para el

MAGIC_NUMBER	  equ 0x1BADB002		;Magic Number para multiboot
FLAGS			  equ 0x0				;Flags para multiboot
CHECKSUM		  equ -MAGIC_NUMBER		;Calcular checksum (magic number + checksum + flags)
KERNEL_STACK_SIZE equ 4096				;Tamano de la pila para el kernel

section .bss
align 4
kernel_stack:
	resb KERNEL_STACK_SIZE ;Reservar memora para la stack

section .text					;Seccion .code
align 4							;El codigo tiene que estar alineado a 4 bytes
	dd MAGIC_NUMBER				;Escribir el magic number a codigo maquina
	dd FLAGS					;Escribir las flags a codigo maquina
	dd CHECKSUM					;Escribir Checksum a codigo maquina

loader:					;Se define en punto de entrada
	extern kmain
	mov esp, kernel_stack + KERNEL_STACK_SIZE	;Apuntar esp al inicio de la stack
	call kmain
.loop:							;Se define loop
	jmp .loop					;Loop infinito
