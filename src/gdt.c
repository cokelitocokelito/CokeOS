/*
This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gdt.h"

u64 gdt_table[3];

u64 create_descriptor(u32 base, u32 limit, u16 flag)
{
	u64 descriptor;

	descriptor = limit & 0x000F0000;
	descriptor |= (flag << 8) & 0x00F0FF00;
	descriptor |= (base >> 16) & 0x000000FF;
	descriptor |= base & 0xFF000000;

	descriptor <<= 32;

	descriptor |= base << 16;
	descriptor |= limit & 0x0000FFFF;

	return descriptor;
}

void init_gdt()
{
	gdt_table[0] = create_descriptor(0x00, 0x00, 0x00);
	gdt_table[1] = create_descriptor(0x00000000, 0xFFFFFFFF, 0x9A);
	gdt_table[2] = create_descriptor(0x00000000, 0xFFFFFFFF, 0x92);

	struct gdt_t gdt_address;

	gdt_address.address = &gdt_table;
	gdt_address.size = 3;

	load_gdt(gdt_address);
}
