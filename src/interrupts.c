/*
This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "interrupts.h"
#include "types.h"

struct idt_entry_t idt[256];

struct idt_entry_t create_entry(u32 offset, u16 segment, u8 DPL)
{
	struct idt_entry_t entry;

	entry.offset_high = (offset >> 16) & 0x00FF;
	entry.offset_low = offset & 0x00FF;

	entry.DPL = 0x8E;
	entry.reserved = 0x00;

	entry.segment_selector = segment;

	return entry;
}

void init_idt()
{
	idt[0] = create_entry(0xDEADBEEF, 0x00, 0x8E);
}
