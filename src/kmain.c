/*
This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "types.h"
#include "Framebuffer.h"
#include "string.h"
#include "COMPort.h"
#include "stdio.h"
#include "gdt.h"

/*
*	La funcion de ese archivo es proporcionar
*	un entrypoint para nuestro kernel (Joix)
*	Author: Jorge Bravo
*/

extern void kmain()
{
	struct framebuffer_character_t chara;
	chara.Colors = FB_DEFAULT_COLOR;
	chara.ASCIICode = ' ';

	framebuffer_init(chara);

	write("Iniciando JOIX...", FD_FRAMEBUFFER);
	write("Iniciado!\n", FD_FRAMEBUFFER);

	write("Iniciando COM1...", FD_FRAMEBUFFER);
	com_configure_line(SERIAL_COM1_BASE);
	write("Iniciado!\n", FD_FRAMEBUFFER);

	write("Intentando escribir a COM1...", FD_FRAMEBUFFER);
	write("Probando COM1", FD_COM1);
	write("Listo!\n", FD_FRAMEBUFFER);

	write("Iniciando GDT...", FD_FRAMEBUFFER);
	init_gdt();
	write("Iniciado!\n", FD_FRAMEBUFFER);
}
