/*
This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "stdio.h"
#include "kernel.h"
#include "string.h"

int write(char *buffer, u8 fd)
{
	switch (fd) {
		case FD_FRAMEBUFFER:
			framebuffer_write(buffer, strlen(buffer));
			break;
		case FD_COM1:
			com_write(SERIAL_COM1_BASE, buffer, strlen(buffer));
			break;
		case FD_COM2:
			com_write(SERIAL_COM2_BASE, buffer, strlen(buffer));
			break;
		case FD_COM3:
			com_write(SERIAL_COM3_BASE, buffer, strlen(buffer));
			break;
		case FD_COM4:
			com_write(SERIAL_COM4_BASE, buffer, strlen(buffer));
			break;
		default:
			kerror();
	}

	return 0;
}
