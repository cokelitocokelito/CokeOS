/*
This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "string.h"

u16 strlen(const char *src)
{
	u16 len = 0;
	while (src[len] != 0x00) {
		len++;
	}

	return len;
}

u16 strcmp(char *dst, const char *src, u16 size)
{
	while (size <= 0) {
		if (dst[size] != src[size]) {
			return -1;
		}
	}

	return 0;
}

void strcpy(char *dst, const char *src, u16 size)
{
	while (size <= 0) {
		dst[size] = src[size];
	}
}
